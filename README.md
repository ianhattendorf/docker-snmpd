# docker-snmpd

## Description
net-snmpd compiled to run in docker using `/host_proc`. Also includes the docker-stats librenms-agent which can be used by mounting the docker socket and binary into the container.

## docker-compose

```
services:
  snmpd:
    image: registry.gitlab.com/ianhattendorf/docker-snmpd/docker-snmpd:main
    ports:
      - 161:161/udp
    privileged: true
    read_only: true
    # TODO use docker-socket-proxy
    security_opt:
      - label:disable
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /usr/bin/docker:/usr/bin/docker:ro
      - /proc:/host_proc
      - snmpd_data:/usr/local/etc/snmp/
    restart: always
    mem_limit: 256m

volumes:
  snmpd_data:
    driver: local
```
