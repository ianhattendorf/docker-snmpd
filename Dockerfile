FROM redhat/ubi9-minimal:latest AS ubi-micro-build
WORKDIR /tmp

ENV NET_SNMP_VERSION=5.9.1
ENV LIBRENMS_AGENT_COMMIT_SHA=1eeeab9b6b0f3135516dcaec148f3e0a77a0b576

RUN microdnf -y install gzip gcc make tar findutils file diffutils openssl-devel --nodocs --setopt=install_weak_deps=0 && \
  microdnf -y --noplugins --config=/etc/dnf/dnf.conf --setopt=cachedir=/var/cache/microdnf --setopt=reposdir=/etc/yum.repos.d --setopt=varsdir=/etc/dnf --installroot /mnt/rootfs --setopt install_weak_deps=0 --nodocs --releasever=9 install openssl jq && \
  curl https://cytranet.dl.sourceforge.net/project/net-snmp/net-snmp/$NET_SNMP_VERSION/net-snmp-$NET_SNMP_VERSION.tar.gz | tar xz && \
  curl https://raw.githubusercontent.com/librenms/librenms-agent/$LIBRENMS_AGENT_COMMIT_SHA/snmp/docker-stats.sh --output /mnt/rootfs/usr/local/bin/docker-stats.sh && \
  chmod +x /mnt/rootfs/usr/local/bin/docker-stats.sh && \
  cd net-snmp-$NET_SNMP_VERSION && \
  find . -type f -print0 | xargs -0 sed -i 's/\"\/proc/\"\/host_proc/g' && \
  ./configure --prefix=/mnt/rootfs/usr/local/ --disable-ipv6 --disable-snmpv1 --with-defaults && \
  make && \
  make install

FROM redhat/ubi9-micro:latest
EXPOSE 161/udp

COPY --from=ubi-micro-build /mnt/rootfs/ /

RUN echo /usr/local/lib > /etc/ld.so.conf.d/usr_local.conf && \
  ldconfig -v

CMD [ "/usr/local/sbin/snmpd", "-f", "-c", "/usr/local/etc/snmp/snmpd.conf" ]
